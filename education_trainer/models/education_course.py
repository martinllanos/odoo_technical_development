from odoo import fields, models

class Course(models.Model):
    _inherit = 'education.course'

    training_implementation_date = fields.Date()