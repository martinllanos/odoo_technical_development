from odoo import fields, models

class Trainer(models.Model):
    _name = 'course.trainer'

    trainer_id = fields.Many2one('res.partner', string='Trainers')
    image = fields.Binary(string='Profile Picture')
    name = fields.Char(related='trainer_id.name', string='Trainer Name')
    course_ids = fields.Many2many('education.course', string='Courses')