{
    'name': 'Education Trainer',
    'description': 'Pengelolaan Course Instructor',
    'author': 'Brainmatics',
    'depends': ['education_app'],
    'data': ['views/course_view.xml',
        'views/trainer_menu.xml',
        'views/trainer_view.xml'
    ],
    'application': False,
}