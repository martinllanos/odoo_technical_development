from odoo import fields, models, api

class Purchase(models.Model):
    _inherit = 'purchase.order'

    vendor_phone = fields.Char(string='Phone', related='partner_id.phone')
    vendor_email = fields.Char(string='Email', related='partner_id.email')
    pic_id = fields.Many2one('res.users', string='Person in Contact', default=lambda self: self.env.uid)

    # def active_user(self):
    #     print(self.env.uid)
    #     return self.env.uid
    # set default active user odoo, https://stackoverflow.com/questions/41318350/how-to-store-the-current-logged-user-id-or-active-user-id-in-odoo-9

    # pic_id = fields.Many2one('res.users', string='Person in Contact', default=active_user)

    down_payment = fields.Float(string='Down Payment')
    total = fields.Float(string='Total', compute='total_calculation', readonly=True)

    @api.model
    def create(self, vals):
        print("\n")
        print("===create===")
        if not vals.get('partner_ref'):
            vals.update({'partner_ref': 'No Vendor Reference'})
        print("\n")
        res = super(Purchase, self).create(vals)
        return res
    
    def write(self, vals):
        res = super(Purchase, self).write(vals)
        print("\n")
        print("===write===")
        for po in self:
            if not po.partner_ref:
                po.partner_ref = 'No Vendor Reference'
        print("\n")
        return res

    @api.depends('amount_total')
    def total_calculation(self):
        print("\n")
        print("===down payment===")
        for po in self:
            po.total = po.amount_total + po.down_payment
        print("\n")

