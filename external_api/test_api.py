from xmlrpc import client

#akses API untuk public, tidak perlu autentifikasi
server = 'http://localhost:8069'
common = client.ServerProxy('%s/xmlrpc/2/common' %server)
print("\n")
print('Odoo Version = ', common.version())
print("\n")

#akses API dengan autentifikasi
db = 'testing'
user, pwd = 'admin', 'admin'
uid = common.authenticate(db, user, pwd, {})
print('Uid = ', uid)

api = client.ServerProxy('%s/xmlrpc/2/object' %server)
count_partner = api.execute_kw(db, uid, pwd, 'res.partner', 'search_count', [[]])
print('Count Partner = ', count_partner)
print('\n')

#search dan read
domain = [('is_company', '=', True)]
company_partner_id = api.execute_kw(db, uid, pwd, 'res.partner', 'search', [domain])
print('Company Partner id = ', company_partner_id)
get_data = api.execute_kw(db, uid, pwd, 'res.partner', 'read', [company_partner_id],
                          {'fields': ['id', 'name', 'phone', 'email']})
for data in get_data:
    print(data)

get_data = api.execute_kw(db, uid, pwd, 'res.partner', 'search_read', [domain],
                          {'fields': ['id', 'name', 'phone', 'email']})
print(get_data)

#CRUD
# create_id = api.execute_kw(db, uid, pwd, 'res.partner', 'create', [{'name': 'Doni Irawan'}])
# write = api.execute_kw(db, uid, pwd, 'res.partner', 'write', [[create_id], {'name': 'Doni Irawan'}])
# read = api.execute_kw(db, uid, pwd, 'res.partner', 'read', [[create_id], {'id': 'Doni Irawan'}])
# delete = api.execute_kw(db, uid, pwd, 'res.partner', 'unlink', [[create_id]])