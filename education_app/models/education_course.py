from odoo import fields, models

class Course(models.Model):
    _name = 'education.course'

    name = fields.Char('Course Name', required=True)
    price = fields.Float('Price')
    course_category = fields.Selection([('development','Development'),
                                        ('networking','Networking'),
                                        ('management','Management')])
    image = fields.Binary('Cover')
    active = fields.Boolean(default=True)

    def button_archive(self):
        print("\n \n")
        traner_obj = self.env['library.member'].search([('name','like','Aziz')])
        print("Trainer Object = ", traner_obj)
        print("\n \n")
