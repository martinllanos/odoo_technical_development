from odoo import http
from odoo.exceptions import Warning 

class Books(http.Controller):

    @http.route('/library/books', auth='user')
    def list(self, **kwargs):
        Book = http.request.env['library.book']
        # print("")
        # print("")
        # print(books)
        # print("")
        # print("")
        # return http.request.render('library_app.book_list_template', {'books': books})
        books = Book.search([])
        if books:
            return http.request.render('library_app.book_list_template', {'books': books})
        else:
            http.Response.status = '400 Bad Request'