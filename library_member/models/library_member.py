from odoo import fields, models

class Member(models.Model):
    _name = 'library.member'
    _description = 'Library Member'

    card_number = fields.Char()
    partner_id = fields.Many2one(
        'res.partner',
        delegate=True,
        ondelete='cascade',
        required=True
    )

#delegate = otomatis membuat data di res.partner
#ondelete cascade = otomatis menghapus data di library.member apa bila data di res.partner dihapus